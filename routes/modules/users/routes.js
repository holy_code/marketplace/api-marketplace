module.exports = {
  create: '/user',
  read: '/users',
  remove: '/user/:id',
  update: '/user/:id',
  login: '/user/login',
};
