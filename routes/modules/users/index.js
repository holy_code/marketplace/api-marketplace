const router = require('koa-better-router')().loadMethods();
const { users } = require.main.require('./logic');
const routes = require('./routes');

router.post(routes.create, ctx => (
  users.create(Object.assign({}, ctx.request.body))
    .then(data => {
      ctx.body = data;
    })
));

router.get(routes.read, ctx => (
  users.read(Object.assign({}, { token: ctx.state.user }, ctx.request.query))
    .then(data => {
      ctx.body = data;
    })
));

router.delete(routes.remove, ctx => (
  users.remove(Object.assign({}, { token: ctx.state.user }, ctx.params))
    .then(data => {
      ctx.body = data;
    })
));

router.put(routes.update, ctx => (
  users.update(Object.assign({}, { token: ctx.state.user }, ctx.params, ctx.request.body))
    .then(data => {
      ctx.body = data;
    })
));

router.post(routes.login, ctx => (
  users.login(Object.assign({}, ctx.request.body))
    .then(data => {
      ctx.body = data;
    })
));

module.exports = router;
