const tables = require('./tables');
const mysql = require('./mysql')();

module.exports = Object.assign({}, tables, { mysql });
