const mysql = require('../../mysql')();
const model = {};
const tableName = 'users';
  
model.insert = parameters => mysql.insert(tableName, parameters);  

model.update = ({ 
  id,
  name,
  last_name,
  mobile,
  email,
  password,
  creator_user_id,
  creation_date,
  active,
  active_date,
}) => {
  const promises = [];

  if (name || name === 0) { promises.push(mysql.update(tableName, { name }, { id })); }
  if (last_name || last_name === 0) { promises.push(mysql.update(tableName, { last_name }, { id })); }
  if (email || email === 0) { promises.push(mysql.update(tableName, { email }, { id })); }
  if (mobile || mobile === 0) { promises.push(mysql.update(tableName, { mobile }, { id })); }
  if (password || password === 0) { promises.push(mysql.update(tableName, { password }, { id })); }
  if (creator_user_id || creator_user_id === 0) { promises.push(mysql.update(tableName, { creator_user_id }, { id })); }
  if (creation_date || creation_date === 0) { promises.push(mysql.update(tableName, { creation_date }, { id })); }
  if (active || active === 0) { promises.push(mysql.update(tableName, { active }, { id })); }
  if (active_date || active_date === 0) { promises.push(mysql.update(tableName, { active_date }, { id })); }

  return Promise.all(promises).then(result => result);
};
module.exports = model;
