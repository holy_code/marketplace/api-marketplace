const insertUpdate = require('./insert_update');
const queries = require('./queries');

module.exports = Object.assign({},
  insertUpdate,
  queries);
