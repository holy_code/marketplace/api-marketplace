const mysql = require('../../mysql')();
const model = {};

model.get = parameters => {  
  let value_parameters = Object.values(parameters);
  let where_conditions = '';
  for (let propertyName in parameters) {
      where_conditions += `${propertyName} = ? AND `;
  }

  const query = `
  SELECT id, name, last_name, mobile, email, password, creator_user_id, creation_date, active, active_date
  FROM users 
  WHERE ${where_conditions} active = 1;`;
  return mysql.query(query, value_parameters);
}

model.getById = id => mysql.query(`
SELECT id, name, last_name, mobile, email, password, creator_user_id, creation_date, active, active_date
FROM users 
WHERE id = ? AND active = 1;
`, [id]);


model.getByEmail = email => mysql.query(`
SELECT u.id, u.name, u.last_name, u.mobile, u.email, u.password, u.creator_user_id, u.creation_date, u.active, u.active_date
FROM users AS u
WHERE u.email = (?) AND u.active = 1
LIMIT 1;`, [email]);

model.getByName = name => mysql.query(`
  SELECT id FROM users WHERE CONCAT_WS(' ',name, last_name)
  LIKE ? AND active = 1;`, [`%${name}%`]);

module.exports = model;
