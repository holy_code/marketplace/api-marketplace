module.exports = {
  "extends": "airbnb",
  "env": {
    "node": true,
    "browser": true
  },
  "parserOptions": {
    "ecmaVersion": 2018
  },
  "rules": {
    "camelcase": "on",
    "no-mixed-operators": "off",
    "prefer-template": "off",
    "no-param-reassign": "off",
    "max-len": ["warn", 250, { "ignoreComments": true }],
    "linebreak-style": "off",
    "import/newline-after-import": "off",
    "arrow-parens": ["error", "as-needed"],
    "newline-per-chained-call": "off",
    "import/no-unresolved": "off",
    "new-cap": "off",
    "no-multi-assign": "off",
    "object-curly-newline": "off"
  }
}