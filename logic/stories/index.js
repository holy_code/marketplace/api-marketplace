const system = require('./system');
const users = require('./users'); 

module.exports = Object.assign({},
  { system },
  { users },
);