const nodemailer = require('nodemailer');
const template = require('./template');

const transporter = nodemailer.createTransport({
  host: 'smtp.office365.com',
  port: 587,
  secure: false,
  auth: {
    user: 'masanchez@holy-code.com',
    pass: '',
  },
  tls: {
    ciphers: 'SSLv3',
  },
  requireTLS: true,
  pool: true,
});

const sendEmail = parameters => {
  parameters.to = parameters.to || '';
  parameters.subject = parameters.subject || '';
  parameters.title = parameters.title || '';
  parameters.text = parameters.text || '';
  parameters.from = parameters.from || 'Holy-Code <masanchez@holy-code.com>';
  parameters.bbc = parameters.bbc || 'masanchez@holy-code.com';
  const html = template(parameters);
  parameters.html = parameters.html || html;
  parameters.watchHtml = parameters.watchHtml || html;
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
  return transporter.sendMail(parameters, error => ((error) ? console.log(error) : parameters.subject));
};

module.exports = { sendEmail };
