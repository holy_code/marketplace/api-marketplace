const jwt = require('jsonwebtoken');
const { users } = require.main.require('./database');
const { messages, keys } = require.main.require('./configurations');
const system = require('../../system');

const functions = {};

function validateEmail(emailToValidate) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(emailToValidate);
}

functions.doubleCheckEmail = parameters => (validateEmail(parameters.email) ? parameters : system.throwError(400, messages.validationError));

functions.checkIfUserExists = parameters => users.get({email: parameters.email})
  .then(foundUser => ((foundUser[0]) ? system.throwError(400, messages.userExists) : parameters));

functions.encryptPassword = parameters => system.encryptPassword(parameters.password)
  .then(encryptedPassword => {    
    parameters.password = encryptedPassword;
    parameters.active = 1;    
    return parameters;
});

functions.insert = parameters => users.insert(parameters)
  .then(result => {
    parameters.id = result.insertId;
    return parameters;
});

functions.createToken = parameters => {  
  const { id, name, last_name, email } = parameters;
  const token = { id, name, last_name, email };
  return ({ token: jwt.sign(token, keys.jwt, { expiresIn: '365d' }) });
}

module.exports = functions;
