const joi = require('@hapi/joi')
const { messages } = require.main.require('./configurations');
const system = require('../../system');
const validation = {};

validation.validateParameters = parameters => {
  const schema = joi.object().keys({
    name: joi.string().trim().min(1).max(255).required(),
    last_name: joi.string().trim().min(1).max(255).required(),
    mobile: joi.string().trim().min(1).max(255).optional(),
    email: joi.string().email({ minDomainSegments: 2 }).required(),
    password: joi.string().trim().min(1).max(255).required(),
    creator_user_id: joi.number().min(0).required(),
    creation_date: joi.date().required(),
    active_date: joi.date().required(),
  });

  try {
    schema.validate(parameters);    
    return parameters;
  }
  catch (err) {     
    system.throwError(400, messages.validationError, err)
  }
};

module.exports = validation;