const functions = require('./functions');
const validation = require('./validation');
const system = require('../../system');

const create = parameters => system.startPromiseChain(parameters)
  .then(validation.validateParameters)
  .then(functions.doubleCheckEmail)
  .then(functions.checkIfUserExists)
  .then(functions.encryptPassword)
  .then(functions.insert)
  .then(functions.createToken);

module.exports = { create };
