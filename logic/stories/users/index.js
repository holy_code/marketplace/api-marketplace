const create = require('./create');
const remove = require('./remove');
const read = require('./read');
const update = require('./update');
const login = require('./login');

module.exports = Object.assign({},
  create,
  remove,
  read,
  update,
  login,
);
 