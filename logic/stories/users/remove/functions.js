const { users } = require.main.require('./database');
const system = require('../../system');

const functions = {};

functions.formatRemove = parameters => {
  delete parameters.token;
  return parameters;
}

functions.remove = parameters => users.update({ id: parameters.id, active: 0 })
  .then(result => result);

module.exports = functions;
