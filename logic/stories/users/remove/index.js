const functions = require('./functions');
const validation = require('./validation');
const system = require('../../system');

const remove = parameters => system.startPromiseChain(parameters)
  .then(validation.validateParameters)
  .then(functions.formatRemove)
  .then(functions.remove);

module.exports = { remove };
