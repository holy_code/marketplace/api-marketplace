const joi = require('@hapi/joi')
const { messages } = require.main.require('./configurations');
const system = require('../../system');
const validation = {};

validation.validateParameters = parameters => {
  const schema = joi.object().keys({
     token: joi.object().required(),
     id: joi.number().min(0).required(),
 });
  const { error, value } = joi.validate(parameters, schema);
  return (error !== null) ? system.throwError(400, messages.validationError, error) : value;
};

module.exports = validation;
