const jwt = require('jsonwebtoken');
const { messages, keys } = require.main.require('./configurations');
const system = require('../../system');
const { users } = require.main.require('./database');

const functions = {};

functions.getUser = parameters => users.getByEmail(parameters.email)
  .then(obtainedUser => ((obtainedUser) ? Object.assign({}, parameters, { obtainedUser: obtainedUser[0] }) : system.throwError(400, messages.loginError)));

functions.checkIfUserExists = parameters => ((parameters.obtainedUser) ? parameters : system.throwError(400, messages.loginError));

functions.checkUserAndPassword = parameters => system.comparePassword(parameters.password, parameters.obtainedUser.password)
  .then(match => ((match) ? parameters : system.throwError(400, messages.loginError)));

functions.setTokenParameters = parameters => ({
  id: parameters.obtainedUser.id,
  name: parameters.obtainedUser.name,
  last_name: parameters.obtainedUser.last_name,
  email: parameters.obtainedUser.email,
  owner_company_id: parameters.obtainedUser.owner_company_id,
  business_name: parameters.obtainedUser.business_name,
});

functions.createToken = parameters => ({ token: jwt.sign(parameters, keys.jwt, { expiresIn: '365d' }) });

module.exports = functions;
