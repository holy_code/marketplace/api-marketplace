const joi = require('@hapi/joi')
const { messages } = require.main.require('./configurations');
const system = require('../../system');
const validation = {};

validation.validateParameters = parameters => {
  const schema = joi.object().keys({
    email: joi.string().trim().max(255).required(),
    password: joi.string().trim().max(255).required(),
  });
  const { error, value } = joi.validate(parameters, schema);
  return (error !== null) ? system.throwError(400, messages.loginError, error) : value;
};

validation.checkUserRolAndCompany = parameters => {
  const params = {
    owner_company_id: parameters.obtainedUser.owner_company_id,
    rol_id: parameters.obtainedUser.rol_id
  }
  const schema = joi.object().keys({
    owner_company_id: joi.number().min(0).required(),
    rol_id: joi.number().min(0).required(),
  });
  const { error } = joi.validate(params, schema);
  return (error !== null) ? system.throwError(400, messages.loginPermissionError, error) : parameters;
}

module.exports = validation;
