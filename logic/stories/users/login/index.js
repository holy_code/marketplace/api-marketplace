const functions = require('./functions');
const validation = require('./validation');
const system = require('../../system');

const login = parameters => system.startPromiseChain(parameters)
  .then(validation.validateParameters)
  .then(functions.getUser)
  .then(functions.checkIfUserExists)
  .then(functions.checkUserAndPassword)
  .then(validation.checkUserRolAndCompany)
  .then(functions.setTokenParameters)
  .then(functions.createToken);

module.exports = { login };
