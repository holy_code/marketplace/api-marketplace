const functions = require('./functions');
const validation = require('./validation');
const system = require('../../system');

const read = parameters => system.startPromiseChain(parameters)
  .then(validation.validateParameters)
  .then(functions.formatRead)
  .then(functions.read);

module.exports = { read };
