const { users } = require.main.require('./database');
const system = require('../../system');

const functions = {};

functions.formatRead = parameters => {
  delete parameters.token;
  return parameters;
}

functions.read = parameters => users.get(parameters)
  .then(result => result);

module.exports = functions;
