const joi = require('@hapi/joi')
const { messages } = require.main.require('./configurations');
const system = require('../../system');
const validation = {};

validation.validateParameters = parameters => {
  const schema = joi.object().keys({
     token: joi.object().required(),
     id: joi.number().min(0).optional(),
     name: joi.string().trim().min(1).max(255).optional(),
     last_name: joi.string().trim().min(1).max(255).optional(),
     email: joi.string().trim().min(1).max(255).optional(),
     password: joi.string().trim().min(1).max(255).optional(),
     owner_company_id: joi.number().min(0).optional(),
     rol_id: joi.number().min(0).optional(),
     creator_user_id: joi.number().min(0).optional(),
     creation_date: joi.date().optional(),
     active_date: joi.date().optional(),
 });
  const { error, value } = joi.validate(parameters, schema);
  return (error !== null) ? system.throwError(400, messages.validationError, error) : value;
};

module.exports = validation;
