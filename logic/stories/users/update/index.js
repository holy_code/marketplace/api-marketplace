const functions = require('./functions');
const validation = require('./validation');
const system = require('../../system');

const update = parameters => system.startPromiseChain(parameters)
  .then(validation.validateParameters)
  .then(functions.formatUpdate)
  .then(functions.update);

module.exports = { update };
