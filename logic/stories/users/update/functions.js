const { users } = require.main.require('./database');
const system = require('../../system');

const functions = {};

functions.formatUpdate = parameters => {
  delete parameters.token;
  return parameters;
}

functions.update = parameters => users.update(parameters)
  .then(result => result);

module.exports = functions;
