const joi = require('@hapi/joi')
const { messages } = require.main.require('./configurations');
const system = require('../../system');
const validation = {};

validation.validateParameters = parameters => {
  const schema = joi.object().keys({
     token: joi.object().required(),
     id: joi.number().min(0).required(),
     name: joi.string().trim().min(1).max(255).required(),
     last_name: joi.string().trim().min(1).max(255).required(),
     email: joi.string().trim().min(1).max(255).required(),
     password: joi.string().trim().min(1).max(255).required(),
     owner_company_id: joi.number().min(0).required(),
     rol_id: joi.number().min(0).required(),
     creator_user_id: joi.number().min(0).required(),
     creation_date: joi.date().required(),
     active: joi.number().min(0).max(1).required(),
     active_date: joi.date().required(),
 });
  const { error, value } = joi.validate(parameters, schema);
  return (error !== null) ? system.throwError(400, messages.validationError, error) : value;
};

module.exports = validation;
