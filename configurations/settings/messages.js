module.exports = {
  messages: {
    genericError: '¡Vaya! algo salió mal, por favor intenta de nuevo.',
    updateError: 'No pudimos actualizar la información.',
    noResults: 'No se encontraron resultados de tu búsqueda.',
    existsError: 'Ya existe.',
    noExistsError: 'No existe.',
    noPermission: 'No tienes permiso para realizar esta acción.',
    loginError: 'Has ingresado un nombre de usuario o contraseña no válido.',
    loginPermissionError: 'No cuentas con alguna compañia o un perfil',
    validationError: 'Formato incorrecto de parametros.',
    userExists: 'Este correo electrónico ya está registrado.',
    userNotExists: 'Este correo electrónico no está registrado.',
    companyNotExists: 'Esta empresa no existe',
    invalidURL: 'URL caducada.',
    invalidContract: 'El contrato tiene información invalida',
    validUpdateContract: 'El contrato se actualizo correctamente',
    pendingPeriods: 'No hay periodos pendientes por realizar',
    periodExists: 'Este periodo ya esta generado',
    periodNotExists: 'Este periodo no exite',
  },
};

